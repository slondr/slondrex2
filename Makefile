PROJECT = slondrex2
PROJECT_DESCRIPTION = Backend server for my personal website
PROJECT_VERSION = 0.1.0

DEPS = cowboy sqlite3 jsx
dep_cowboy_commit = 2.9.0
dep_sqlite3 = git https://github.com/processone/erlang-sqlite3

include erlang.mk
