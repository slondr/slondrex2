-module(favorites).
-behavior(cowboy_handler).
-export([init/2, options/2, allowed_methods/2, content_types_provided/2, content_types_accepted/2, get_json/2, post_json/2,
	 put_json/2, patch_json/2, delete_resource/2]).

init(Req, State) ->
    {cowboy_rest, Req, State}.

apply_cors(Req) ->
    Req0 = cowboy_req:set_resp_header(<<"access-control-allow-headers">>, <<"content-type">>, Req),
    Req1 = cowboy_req:set_resp_header(<<"access-control-allow-methods">>, <<"GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS">>, Req0),
    Req2 = cowboy_req:set_resp_header(<<"access-control-allow-origin">>, <<"*">>, Req1),
    Req2.
    
options(Req, State) ->
    {ok, apply_cors(Req), State}.
    
allowed_methods(Req, State) ->
    {[<<"GET">>, <<"POST">>, <<"PUT">>, <<"PATCH">>, <<"DELETE">>, <<"HEAD">>, <<"OPTIONS">>], Req, State}.

content_types_provided(Req, State) ->
    {[{{<<"application">>, <<"json">>, '*'}, get_json}], Req, State}.

content_types_accepted(Req, State) ->
    {[{{<<"application">>, <<"json">>, '*'},
       case cowboy_req:method(Req) of
	   <<"POST">> -> post_json;
	   <<"PUT">> -> put_json;
	   <<"PATCH">> -> patch_json
       end}], Req, State}.

convert_tuple_to_map(M) ->
    {Id, Count, Href, Text} = M,
    #{id => Id, count => Count, href => Href, text => Text}.

get_json(Req, State) ->
    whereis(db)!{get, self()},
	    receive {ok, Data} -> 
		    Response = jsx:encode(lists:map(fun convert_tuple_to_map/1, Data)),
		    {Response, apply_cors(Req), State}
	    end.

post_json(Req0, State) ->
    {ok, Body, Req} = cowboy_req:read_body(Req0),
    ParsedBody = jsx:decode(Body),
    whereis(db)!{inc, self(), ParsedBody},
    {true, apply_cors(Req), State}.

put_json(Req0, State) ->
    {ok, Body, Req} = cowboy_req:read_body(Req0),
    ParsedBody = jsx:decode(Body),
    Href = maps:get(<<"href">>, ParsedBody),
    Text = maps:get(<<"text">>, ParsedBody),
    whereis(db)!{add, self(), Href, Text},
    {true, apply_cors(Req), State}.

patch_json(Req0, State) ->
    whereis(db)!{reset, self()},
    {true, apply_cors(Req0), State}.

delete_resource(Req, State) ->
    #{id := Id} = cowboy_req:match_qs([id], Req),
    whereis(db)!{delete, self(), Id},
    {true, apply_cors(Req), State}.
