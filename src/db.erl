-module(db).
-export([start/0]).

start() ->
    sqlite3:open(favorites),
    sqlite3:sql_exec(favorites, "CREATE TABLE IF NOT EXISTS links (
      id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
      count INTEGER,
      href TEXT,
      text TEXT
    )"),
    query().

query() ->
    receive
	{get, From} ->
	    [{columns, _Columns}, {rows, Rows}] = sqlite3:sql_exec(favorites, "select * from links order by count desc"),
	    From!{ok, Rows};
	{add, From, Url, Text}->
	    sqlite3:sql_exec(favorites, "insert into links (count, href, text) values (0, ?, ?)", [Url, Text]),
	    From!{ok};
	{update, From, Id, Url, Text} ->
	    sqlite3:sql_exec(favorites, "update links set href = ?, text = ? where id = ?", [Url, Text, Id]),
	    From!{ok};
	{delete, From, Id} ->
	    sqlite3:sql_exec(favorites, "delete from links where id = ?", [Id]),
	    From!{ok};
	{reset, From} ->
	    sqlite3:sql_exec(favorites, "update links set count = 0"),
	    From!{ok};
	{inc, From, Id} ->
	    sqlite3:sql_exec(favorites, "update links set count = count + 1 where id = ?", [Id]),
	    From!{ok};
	{close} ->
	    sqlite3:close(favorites);
	_ ->
	    io:fwrite("Not Implemented")
    end,
    query().
