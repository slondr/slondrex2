-module(slondrex2_app).
-behaviour(application).

-export([start/2]).
-export([stop/1]).

start(_Type, _Args) ->
    register(db, spawn(db, start, [])),
    Dispatch = cowboy_router:compile([{'_', [{"/zoop", zoop, []},
					     {"/favorites", favorites, []}]}]),
    {ok, _} = cowboy:start_clear(rest_listener, [{port, 8080}], 
				 #{env => #{dispatch => Dispatch}}),
    slondrex2_sup:start_link().

stop(_State) ->
    ok.
