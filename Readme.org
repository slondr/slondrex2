* API specification
** GET /
Hello world
** GET /zoop
You've been 👉😎👉 Zooped!
** GET /zoop/<name>
Like above, but specifically calling out <name>
** GET /favorites
Returns all favorite objects, as an array
** POST /favorites
Update the count given by an href in the request body
** PUT /favorites
Create a new entry in the database, with the given href and text.
** PATCH /favorites
Empty the database.
** DELETE /favorites?id=<id>
Delete the entry with the id =<id>=.
